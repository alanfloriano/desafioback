# Desafio BackEnd

Este projeto foi gerado utilizando NodeJs (https://nodejs.org/en/) versão 12.6.0.
Também foi utilizada as dependencias:
* express
* body-parser
* jsonwebtoken
* mongoose
* morgan (dev log)
* nodemon (dev)

## Instruções de instalação e execução

#### Instalação

Fazer o Download do projeto.
Para instalaçao das dependecias: 

``` npm install ```

#### Execução

##### DEV

O projeto está utilizando o nodemon para visualização de logs e chamadas.
Para iniciar a execução basta digitar 

``` npm start ```

o que irá fazer com que o nodemon inicie a aplicação.

##### PRODUÇÃO

O projeto está com o arquivo de configuração do PM2 (https://pm2.keymetrics.io) para deploy.
Neste caso basta digitar

``` pm2 start ecosystem.config.js --env production ```

ou caso exista a necessidade de realizar o deploy será necessário a configuração do arquivo ecosystem.config.js.

## Endpoints e Requisições.

### Admin

#### POST localhost:3000/admin/signin

Metodo: POST
Endereço do Endpoint: localhost:3000/admin/signin
Endpoint responsavel pelo login do usuário.
Espera receber um json:

```
{
    "usuario": "nome do usuario",
    "senha": "senha do usuario"
}
```

retorna um json contendo informações do usuário e o jwt: 

```
{
    "usuario": {
        "_id": "5e4cdf4c6cb7011cf2d42c6c",
        "dataCriacao": "2020-02-19T07:08:24.000Z"
    },
    "jwtoken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1ZTRjZGY0YzZjYjcwMTFjZjJkNDJjNmMiLCJpYXQiOjE1ODIwOTYyNDYsImV4cCI6MTU4MjE4MjY0Nn0.2YbkQaepp37Dt0GoNsRVMb_L1NnJ4JZHEvqQEzhAkQw"
}
```

#### POST localhost:3000/admin/signup

Metodo: POST
Endereço do Endpoint: localhost:3000/admin/signup
Endpoint responsavel pela criação do usuário.
Espera receber um json:

```
{
    "usuario": "nome do usuario",
    "senha": "senha do usuario"
}
```

retorna uma mensagem sobre a criação do usuario e o seu respectivo id: 

```
{
    "mensagem": "Cadastro realizado com sucesso",
    "user": {
        "_id": "5e4cde0e178eca1c811ad5ea"
    }
}
```

### Animal

#### GET localhost:3000/animal/

Metodo: GET
Endereço do Endpoint: localhost:3000/animal/
Endpoint responsavel pela entrega de uma lista contendo todos animais cadastrados.

retorna um json contendo uma lista de animais cadastrados:

```
{
    "animais": [
        {
            "dateCreation": "2020-02-19T07:08:24.000Z",
            "_id": "5e4ce06a6cb7011cf2d42c6d",
            "tipo": "Bovino",
            "nome": "Boi okay 1",
            "peso": 180,
            "idade": 36
        },
        {
            "dateCreation": "2020-02-19T07:08:24.000Z",
            "_id": "5e4ce07a6cb7011cf2d42c6e",
            "tipo": "Bovino",
            "nome": "Boi 23",
            "peso": 180,
            "idade": 36
        }
    ]
}
```

#### GET localhost:3000/animal/:animalId

Metodo: GET
Endereço do Endpoint: localhost:3000/animal/:animalId
Endpoint responsavel pela entrega das informações referente a um animal especifico.

Retorna um json contendo as informações do animal de acordo com o ID enviado:

```
{
    "animal": {
        "dateCreation": "2020-02-19T07:08:24.000Z",
        "_id": "5e4ce07a6cb7011cf2d42c6e",
        "tipo": "Bovino",
        "nome": "Boi 23",
        "peso": 180,
        "idade": 36
    }
}
```


#### POST localhost:3000/animal/novo_animal

Metodo: POST
Endereço do Endpoint: localhost:3000/animal/novo_animal
Endpoint responsavel por criar uma novo animal no banco de dados.

Espera receber um json contendo nome, peso, tipo e idade:

```
{
    "nome": "Boi 23",
    "tipo": "Bovino",
    "peso": "180",
    "idade": "36"
}
```

Retorna um json contendo uma mensagem e as informações do animal cadastrado:

```
{
    "mensagem": "Animal cadastrado com sucesso",
    "animal": {
        "dateCreation": "2020-02-19T07:08:24.000Z",
        "_id": "5e4ce07a6cb7011cf2d42c6e",
        "tipo": "Bovino",
        "nome": "Boi 23",
        "peso": 180,
        "idade": 36,
        "__v": 0
    }
}
```

#### PATCH localhost:3000/animal/editar_animal/:animalId

Metodo: PATCH
Endereço do Endpoint: localhost:3000/animal/editar_animal/:animalId
Endpoint responsavel pela edição das informações do animal.

Espera receber um json contendo o respectivo dado a ser atualizado:

```
{
    "peso": "180",
    "idade": "36"
}
```

Retorna um json contendo uma mensagem e um objeto contendo as mudanças que foram realizadas:

```
{
    "mensagem": "Animal atualizado com sucesso",
    "changes": {
        "peso": "180",
        "idade": "36"
    }
}
```

#### DELETE localhost:3000/animal/remover_animal/:animalId

Metodo: Delete
Endereço do Endpoint: localhost:3000/animal/remover_animal/:animalId
Endpoint reponsavel por deletar um animal do banco de dados.

Retorna um json contendo uma mensagem e o id que foi deletado:

```
{
    "mensagem": "Animal deletado com sucesso.",
    "_id": "5e4ccf96b8704e17281b1d1d"
}
```