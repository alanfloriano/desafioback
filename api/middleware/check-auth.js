const jwt = require('jsonwebtoken');
const errorFunc = require('../shared/error');

module.exports = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(" ")[1];
        const decoded = jwt.verify(token, process.env.JWT_KEY);
        req.userData = decoded;
        next();
    } catch (err) {
        const error = errorFunc(401, 'Usuário não autenticado', err);
        next(error);
    }
};