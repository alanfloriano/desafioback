const mongoose = require('mongoose');

const adminSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    usuario: {
        type: String,
        required: true,
        unique: true,
    },
    password: {
        type: String,
        required: true
    },
    dateCreation: { type: Date, default: Date(Date.now()) }
});

module.exports = mongoose.model("Admin", adminSchema);