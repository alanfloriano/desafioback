const mongoose = require('mongoose');

const animaSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    tipo: { type: String, required: true },
    nome: { type: String, required: true },
    peso: { type: Number, required: true },
    idade: { type: Number, required: true },
    dateCreation: { type: Date, default: Date(Date.now()) }
});

module.exports = mongoose.model("Animal", animaSchema);