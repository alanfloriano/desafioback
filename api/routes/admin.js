const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const adminModel = require('../models/admin')
const errorFunc = require('../shared/error');

router.post('/signin', (req, res, next) => {
    var usuario = req.body.usuario;
    var senha = req.body.senha;
    adminModel.findOne({ usuario: usuario })
        .exec()
        .then(doc => {
            if (doc) {
                bcrypt.compare(senha, doc.password, (compareErr, compare) => {
                    if (compareErr) {
                        const error = errorFunc(401, 'Usuário e ou senha incorretos', null);
                        next(error);
                    }
                    if (compare) {
                        const token = jwt.sign({
                            userId: doc._id
                        }, process.env.JWT_KEY, {
                            expiresIn: "1 days"
                        });
                        const response = {
                            usuario: {
                                _id: doc._id,
                                dataCriacao: doc.dateCreation
                            },
                            jwtoken: token
                        };
                        return res.status(200).json(response);
                    }
                    const error = errorFunc(401, 'Usuário e ou senha incorretos', null);
                    next(error);
                })
            } else {
                const error = errorFunc(401, 'Usuário e ou senha incorretos', null);
                next(error);
            }
        }).catch(err => {
            const error = errorFunc(500, err.message, err);
            next(error);
        });
});

router.post('/signup', (req, res, next) => {
    bcrypt.hash(req.body.senha, 10, (hashError, hash) => {
        if (hashError) {
            const error = errorFunc(500, hashError.message, null);
            next(error);
        } else {
            const admin = new adminModel({
                _id: new mongoose.Types.ObjectId,
                usuario: req.body.usuario,
                password: hash,
            });
            adminModel.findOne({ usuario: req.body.usuario })
                .exec()
                .then(find => {
                    if (find) {
                        const error = errorFunc(409, 'Usuário já esta cadastrado', null);
                        next(error)
                    }
                    return admin.save();
                }).then(result => {
                    const response = {
                        mensagem: 'Cadastro realizado com sucesso',
                        user: {
                            _id: result._id,
                            user: result.user,
                        }
                    };
                    res.status(201).json(response);
                })
                .catch(err => {
                    if (err.code == 11000) {
                        const error = errorFunc(400, 'Usuário já cadastrado', err);
                        next(error);
                    } else {
                        const error = errorFunc(500, err.message, err);
                        next(error);
                    }
                });
        }
    })
});

module.exports = router;