const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const adminModel = require('../models/admin');
const animalModel = require('../models/animal');
const auth = require('../middleware/check-auth');
const errorFunc = require('../shared/error');


//=========================== GET ALL ANIMALS ===========================
router.get('/', (req, res, next) => {
    animalModel.find()
        .select('_id tipo nome peso idade dateCreation')
        .exec()
        .then(docs => {
            res.status(200).json({
                animais: docs
            });
        }).catch(err => {
            const error = errorFunc(500, err.message, err);
            next(error);
        })
});

//=========================== GET UNIQUE PRODUCT DETAIL ===========================
router.get('/:animalId', (req, res, next) => {
    const animalId = req.params.animalId
    if (mongoose.Types.ObjectId.isValid(animalId)) {
        animalModel.findById(animalId)
            .select('_id tipo nome peso idade dateCreation')
            .exec()
            .then(doc => {
                if (doc) {
                    res.status(200).json({
                        animal: doc
                    });
                } else {
                    const error = errorFunc(404, 'Animal não encontrado', null);
                    next(error);
                }
            })
            .catch(err => {
                const error = errorFunc(500, err.message, err);
                next(error);
            });
    } else {
        const error = errorFunc(404, 'Animal não encontrado', null);
        next(error);
    }
});

//=========================== POST NOVO ANIMAL ===========================
router.post('/novo_animal', auth, (req, res, next) => {
    adminModel.findById({ _id: req.userData.userId })
        .exec()
        .then(doc => {
            if (doc) {
                if (req.body.idade < 0) {
                    res.status(400);
                    const error = errorFunc(400, "Idade do animal não pode ser negativa", null);
                    return next(error);
                } else if (req.body.peso < 0) {
                    res.status(400);
                    const error = errorFunc(400, "Peso do animal não pode ser negativa", null);
                    return next(error);
                }
                const animal = new animalModel({
                    _id: new mongoose.Types.ObjectId,
                    tipo: req.body.tipo,
                    nome: req.body.nome,
                    peso: req.body.peso,
                    idade: req.body.idade
                });
                return animal.save();
            } else {
                res.status(401);
                const error = errorFunc(401, "Usuário não autenticado", null);
                next(error);
            }
        }).then(result => {
            if (res.statusCode == 401 || res.statusCode == 400) {
                return;
            } else {
                res.status(201).json({
                    mensagem: "Animal cadastrado com sucesso",
                    animal: result
                });
            }
        })
        .catch(err => {
            const error = errorFunc(500, err.message, err);
            next(error);
        });
});


//=========================== PATCH EDITAR ANIMAL ===========================
router.patch('/editar_animal/:animalId', auth, (req, res, next) => {
    var animalId = req.params.animalId
    var changes = req.body
    adminModel.findById({ _id: req.userData.userId })
        .exec()
        .then(doc => {
            if (doc) {
                if (changes.hasOwnProperty('peso')) {
                    if (changes.peso < 0) {
                        const error = errorFunc(400, "Peso do animal não pode ser negativa", null);
                        return next(error);
                    }
                }
                if (changes.hasOwnProperty('idade')) {
                    if (changes.idade < 0) {
                        const error = errorFunc(400, "Idade do animal não pode ser negativa", null);
                        return next(error);
                    }
                }
                if (changes.hasOwnProperty('_id')) {
                    const error = errorFunc(400, "Não é possivel alterar o ID do animal.", null);
                    return next(error);
                } else {
                    animalModel.updateOne({ _id: animalId }, changes)
                        .then(result => {
                            if (result.nModified === 1) {
                                res.status(201).json({
                                    mensagem: 'Animal atualizado com sucesso',
                                    changes: changes
                                })
                            } else {
                                const error = errorFunc(202, "Nenhum produto foi modificado", null);
                                next(error);
                            }
                        })
                        .catch(err => {
                            const error = errorFunc(500, err.message, err);
                            next(error);
                        });
                }
            } else {
                const error = errorFunc(401, 'Usuário não autenticado', null);
                next(error);
            }
        }).catch(err => {
            const error = errorFunc(401, 'Usuário não autenticado', err);
            next(error);
        });
});

//=========================== DELETE ANIMAL ===========================
router.delete('/remover_animal/:animalId', auth, (req, res, next) => {
    const animalId = req.params.animalId;
    adminModel.findById({ _id: req.userData.userId })
        .exec()
        .then(doc => {
            if (doc) {
                if (mongoose.Types.ObjectId.isValid(animalId)) {
                    animalModel.find({ _id: animalId })
                        .exec()
                        .then(animal => {
                            if (!animal || animal.length == 0) {
                                res.status(404)
                                const error = errorFunc(404, 'Animal não encontrado', null);
                                return next(error);
                            } else {
                                return animalModel.deleteOne({ _id: animalId }).exec()
                            }
                        })
                        .then(result => {
                            if (res.statusCode === 404) {
                                return;
                            } else {
                                if (result.deletedCount > 0) {
                                    res.status(200).json({
                                        mensagem: 'Animal deletado com sucesso.',
                                        _id: animalId
                                    })
                                } else {
                                    res.status(202).json({
                                        mensagem: 'Nenhum animal foi deletada'
                                    });
                                }
                            }
                        })
                        .catch(err => {
                            const error = errorFunc(500, err.message, err);
                            next(error);
                        })
                } else {
                    const error = errorFunc(404, 'Animal não encontrado', null);
                    next(error);
                }
            } else {
                const error = errorFunc(401, 'Usuário não autenticado', null);
                next(error);
            }
        }).catch(err => {
            const error = errorFunc(500, err.mensagem, err);
            next(error);
        });
});

module.exports = router;