module.exports = function creatingError(status, message, err) {
    const error = new Error(message);
    error.status = status;
    return error;
}
