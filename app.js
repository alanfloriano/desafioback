const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

// Routes imports
const adminRoute = require('./api/routes/admin');
const animalRoute = require('./api/routes/animal');

// Mongoose
mongoose.connect("mongodb+srv://" + process.env.MONGO_ATLAS_DB + ":" +
    process.env.MONGO_ATLAS_PW + process.env.MONGO_ATLAS_COMPLEMENT,
    {
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true
    }
);

// Developer log
app.use(morgan('dev'));

// BodyParser
app.use(bodyParser.urlencoded({ limit: '50mb', extended: false }));
app.use(bodyParser.json({ limit: '50mb' }));

// Headers
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', '*');
    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
        return res.status(200).json({});
    }
    next();
});

//Routes requests
app.use('/admin', adminRoute);
app.use('/animal', animalRoute);

// Handling Errors
app.use((req, res, next) => {
    const error = new Error('Not found');
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    if (Array.isArray(error)) {
        res.status(500);
        listError = [];
        error.forEach(element => {
            let err = {
                status: element.status,
                message: element.message
            };
            listError.push(err);
        });
        res.json({
            error: listError
        });
    } else {
        res.status(error.status || 500);
        res.json({
            error: {
                message: error.message
            }
        });
    }
});

module.exports = app;