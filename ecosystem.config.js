module.exports = {
  apps: [{
    name: 'API',
    script: 'server.js',

    // Options reference: https://pm2.keymetrics.io/docs/usage/application-declaration/
    args: 'one two',
    instances: 1,
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    env: {
      MONGO_ATLAS_PW: "DesafioBackAlan",
      JWT_KEY: "DesafioBackEnd",
      MONGO_ATLAS_DB: "desafioBack",
      MONGO_ATLAS_COMPLEMENT: "@desafioback-y82xo.mongodb.net/test?retryWrites=true&w=majority",
      NODE_ENV: 'development'
    },
    env_production: {
      MONGO_ATLAS_PW: "DesafioBackAlan",
      JWT_KEY: "DesafioBackEnd",
      MONGO_ATLAS_DB: "desafioBack",
      MONGO_ATLAS_COMPLEMENT: "@desafioback-y82xo.mongodb.net/test?retryWrites=true&w=majority",
      NODE_ENV: 'production'
    }
  }],

  deploy: {
    production: {
      user: 'node',
      host: '212.83.163.1',
      ref: 'origin/master',
      repo: 'git@github.com:repo.git',
      path: '/var/www/production',
      'post-deploy': 'npm install && pm2 reload ecosystem.config.js --env production'
    }
  }
};
